#!/bin/sh
# startup script for starting a network bridge and initialize a
# private network that will use the node as its gateway with NAT.
# connections coming from the outside on the main node
# can be redirected on the internal containers using iptables.
# if you want it to start at boot, you can add it like this
# to /etc/rc.d/rc.local
#
# # start a bridge used by lxc containers
# if [ -x /usr/local/sbin/lxc-network-bridge-nat ]; then
#    /usr/local/sbin/lxc-network-bridge-nat
# fi

/sbin/brctl addbr lxcbr0
/sbin/brctl setfd lxcbr0 0
/sbin/ifconfig lxcbr0 192.168.11.1 netmask 255.255.255.0 promisc up
echo 1 > /proc/sys/net/ipv4/ip_forward
echo 1 > /proc/sys/net/ipv4/conf/lxcbr0/proxy_arp
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -I INPUT 1 -i lxcbr0 -j ACCEPT
iptables -I FORWARD 1 -i lxcbr0 -o eth0 -j ACCEPT
iptables -I FORWARD 1 -i eth0 -o lxcbr0 -j ACCEPT

# examples of redirections
#iptables -t nat -A PREROUTING -p tcp --dport 51 -i eth0 -j DNAT --to 192.168.11.2:51
#iptables -t nat -A PREROUTING -p tcp --dport 80 -i eth0 -j DNAT --to 192.168.11.3:80
