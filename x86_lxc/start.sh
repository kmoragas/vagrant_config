#reference: http://docs.drupalvm.com/en/latest/other/vagrant-lxc/

export VAGRANT_DEFAULT_PROVIDER=lxc

sudo modprobe iptable_filter
sudo modprobe ip6table_filter

sudo /sbin/brctl addbr lxcbr0
sudo /sbin/brctl setfd lxcbr0 0
sudo /sbin/ifconfig lxcbr0 192.168.11.1 netmask 255.255.255.0 promisc up
echo 1 | sudo tee -a /proc/sys/net/ipv4/ip_forward
echo 1 | sudo tee -a /proc/sys/net/ipv4/conf/lxcbr0/proxy_arp
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -I INPUT 1 -i lxcbr0 -j ACCEPT
sudo iptables -I FORWARD 1 -i lxcbr0 -o eth0 -j ACCEPT
sudo iptables -I FORWARD 1 -i eth0 -o lxcbr0 -j ACCEPT

# examples of redirections
#iptables -t nat -A PREROUTING -p tcp --dport 51 -i eth0 -j DNAT --to 192.168.11.2:51
#iptables -t nat -A PREROUTING -p tcp --dport 80 -i eth0 -j DNAT --to 192.168.11.3:80
