BOX_SETUP_SCRIPT = <<END
#!/bin/bash

sudo apt-get update
sudo apt-get install -y lxc lxc-templates cgroup-lite redir bridge-utils

cat << EOF > /tmp/lxcbr1.cfg
auto eth1
iface eth1 inet manual
        up ifconfig eth1 promisc up
        down ifconfig eth1 promisc down

auto lxcbr1
iface lxcbr1 inet static
    address 192.168.42.10
    broadcast 192.168.42.255
    netmask 255.255.255.0
    bridge_ports eth1
    bridge_stp off
    bridge_waitport 0
    bridge_fd 0
EOF

sudo mv /tmp/lxcbr1.cfg /etc/network/interfaces.d/lxcbr1.cfg

sudo service networking restart
sudo ifup lxcbr1

cd /tmp
wget -q https://dl.bintray.com/mitchellh/vagrant/vagrant_1.6.5_x86_64.deb

sudo dpkg -i vagrant_1.6.5_x86_64.deb
vagrant plugin install vagrant-lxc
END
